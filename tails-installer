#!/usr/bin/python -tt
# coding: utf-8
#
# Copyright © 2008-2013  Red Hat, Inc. All rights reserved.
# Copyright © 2012-2016  Tails Developers <tails@boum.org>
#
# This copyrighted material is made available to anyone wishing to use, modify,
# copy, or redistribute it subject to the terms and conditions of the GNU
# General Public License v.2.  This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY expressed or implied, including the
# implied warranties of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.  You should have
# received a copy of the GNU General Public License along with this program; if
# not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA. Any Red Hat trademarks that are
# incorporated in the source code or documentation are not subject to the GNU
# General Public License and may only be used or replicated with the express
# permission of Red Hat, Inc.
#
# Author(s): Luke Macken <lmacken@redhat.com>
#            Tails Developers <tails@boum.org>

import os
import sys

from tails_installer import _

def parse_args():
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('-c', '--console', dest='console', action='store_true',
                      help='Use console mode instead of the GUI')
    parser.add_option('-f', '--force', dest='force', action='store',
                      type='string', help='Force the use of a given drive',
                      metavar='DRIVE')
    parser.add_option('-s', '--safe', dest='safe', action='store_true',
                      help='Use the "safe, slow and stupid" bootloader')
    parser.add_option('-n', '--noverify', dest='noverify', action='store_true',
                      help='Skip checksum verification')
    parser.add_option('-v', '--verbose', dest='verbose', action='store_true',
                      help='Output extra debugging messages')
    parser.add_option('-u', '--unprivileged', dest='unprivileged',
                      action='store_true', help='Allow not being root')
    parser.add_option('-k', '--extra-kernel-args', dest='kernel_args',
                      action='store', metavar='ARGS',
                      help='Supply extra kernel arguments'
                           ' (eg: -k noswap,selinux=0,elevator=noop)')
    parser.add_option('-m', '--reset-mbr', dest='reset_mbr',
                      action='store_true', default=False,
                      help='Reset the Master Boot Record')
    parser.add_option('-C', '--device-checksum', dest='device_checksum',
                      action='store_true', default=False,
                      help='Calculate the SHA1 of the device')
    parser.add_option('-L', '--liveos-checksum', dest='liveos_checksum',
                      action='store_true', default=False,
                      help='Calculate the SHA1 of the Live OS')
    parser.add_option('-H', '--hash', dest='hash',
                      action='store', metavar='HASH', default='sha1',
                      help='Use a specific checksum algorithm (default: sha1)')
    parser.add_option('--clone', dest='clone', action='store_true',
                      help='Clone the currently running Live system')
    #parser.add_option('-F', '--format', dest='format', action='store_true', default=False,
    #                  help='Format the device as FAT32 (WARNING: destructive)')
    parser.add_option('-P', '--partition', dest='partition', action='store_true', default=False,
                      help='Partition the device')
    #parser.add_option('-z', '--usb-zip', dest='zip', action='store_true',
    #                  help='Initialize device with zipdrive-compatible geometry'
    #                        ' for booting in USB-ZIP mode with legacy BIOSes. '
    #                        'WARNING:  This will erase everything on your '
    #                        'device!')

    # Let's override argv to effectively ignore the command-line
    # arguments until #8861 is solved. We do this (as opposed to
    # removing this code) to cheaply ensure we get the same default
    # options set.
    override_argv = []
    # But for our own debugging purposes we still allow these:
    if '--verbose' in sys.argv or os.getenv('DEBUG') == '1':
        override_argv.append('--verbose')
    return parser.parse_args(override_argv)


def main():
    opts, args = parse_args()
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    from tails_installer.gui import TailsInstallerWindow
    try:
        win = TailsInstallerWindow(opts=opts, args=sys.argv)
        Gtk.main()
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()
